package com.mastertech.cadempresa.producers;

import com.mastertech.cadempresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec2-william-pimentel-2", empresa);
        System.out.println("Empresa " + empresa.getNome() + " enviada com sucesso! CNPJ: " + empresa.getCnpj());
    }
}
