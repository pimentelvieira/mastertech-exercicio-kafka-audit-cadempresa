package com.mastertech.cadempresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechExercicioKafkaAuditCadempresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechExercicioKafkaAuditCadempresaApplication.class, args);
	}

}
