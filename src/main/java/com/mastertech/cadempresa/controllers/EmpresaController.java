package com.mastertech.cadempresa.controllers;

import com.mastertech.cadempresa.producers.EmpresaProducer;
import com.mastertech.cadempresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmpresaController {

    @Autowired
    private EmpresaProducer producer;

    @PostMapping("/empresa")
    public void cadastrar(@RequestBody Empresa empresa) {
        producer.enviarAoKafka(empresa);
    }
}
